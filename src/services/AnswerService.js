import axios from 'axios'

class AnswerService {
    getAll() {
        return axios.get(`http://localhost:8080/answer`);
    }

    getAnswersByQuestion(questionId) {
        return axios.get(`http://localhost:8080/answer/byQuestion/${questionId}`);
    }

    getAnswersByUser(userId) {
        return axios.get(`http://localhost:8080/answer/byUser/${userId}`);
    }

    getAnswer(id) {
        return axios.get(`http://localhost:8080/answer/${id}`);
    }

    deleteAnswer(id){
        return axios.delete(`http://localhost:8080/answer/${id}`);
    }

    saveAnswer(answer) {
        return axios.post(`http://localhost:8080/answer`, answer);
    }

    updateAnswer(answer) {
        return axios.put(`http://localhost:8080/answer`, answer);
    }
}

export default new AnswerService();