import axios from 'axios'


class QuestionService {
    getAll() {
        return axios.get(`http://localhost:8080/question`)
    }

    getQuestionsByUser(userId) {
        return axios.get(`http://localhost:8080/question/byUser/${userId}`)
    }

    getQuestion(id) {
        return axios.get(`http://localhost:8080/question/${id}`);
    }

    deleteQuestion(id){
        return axios.delete(`http://localhost:8080/question/${id}`);
    }

    saveQuestion(question) {
        return axios.post(`http://localhost:8080/question`, question);
    }

    updateQuestion(question) {
        return axios.put(`http://localhost:8080/question`, question);
    }
}

export default new QuestionService();