import axios from 'axios'

class TagService {
    getAll() {
        return axios.get(`http://localhost:8080/tag`)
    }

    getTag(id) {
        return axios.get(`http://localhost:8080/tag/${id}`);
    }

    deleteTag(id){
        return axios.delete(`http://localhost:8080/tag/${id}`);
    }

    saveTag(tag) {
        return axios.post(`http://localhost:8080/tag`, tag);
    }

    updateTag(tag) {
        return axios.put(`http://localhost:8080/tag`, tag);
    }
}

export default new TagService();