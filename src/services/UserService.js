import axios from 'axios'

class UserService {
    login(loginRequest) {
        return axios.post(`http://localhost:8080/user/login`, loginRequest);
    }

    getAll() {
        return axios.get(`http://localhost:8080/user`)
    }

    getUser(id) {
        return axios.get(`http://localhost:8080/user/${id}`);
    }

    deleteUser(id){
        return axios.delete(`http://localhost:8080/user/${id}`);
    }

    saveUser(user) {
        return axios.post(`http://localhost:8080/user`, user);
    }

    updateUser(user) {
        return axios.put(`http://localhost:8080/user`, user);
    }
}

export default new UserService();