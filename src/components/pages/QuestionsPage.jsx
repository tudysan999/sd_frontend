import React, { useContext, useState, useEffect } from "react";
import { Header, Button, Grid, Dimmer, Loader } from "semantic-ui-react";
import { UserContext } from "../../resources/UserContext";
import AnswerService from "../../services/AnswerService";
import QuestionService from "../../services/QuestionService";
import QuestionFormPanel from "../UI/questionsPage/QuestionFormPanel";
import QuestionList from "../UI/questionsPage/QuestionList";
import QuestionFilter from "../UI/questionsPage/QuestionFilter";
import TagService from "../../services/TagService";
import { toast, ToastContainer } from "react-toastify";

const QuestionsPage = () => {
	const { user } = useContext(UserContext);
	const [questions, setQuestions] = useState([]);
	const [isFormOpen, setIsFormOpen] = useState(false);
	const [isLoading, setIsLoading] = useState(false);
	const [initialQuestions, setInitialQuestions] = useState([]);
	const [tags, setTags] = useState([]);

	const sortByDate = (list) => {
		return list.sort((a, b) => {
			return new Date(b.date).getTime() - new Date(a.date).getTime();
		});
	};
// eslint-disable-next-line
	useEffect(async () => {
		try {
			setIsLoading(true);

			const [questionsR, tagsR] = await Promise.all([
				QuestionService.getAll(),
				TagService.getAll(),
			]);

			setTags(tagsR.data);
			let existingQuestions = questionsR.data;

			for (let question of existingQuestions) {
				const answerR = await AnswerService.getAnswersByQuestion(
					question.id
				);

				question.answerCount = answerR.data.length;
			}

			setQuestions(sortByDate(existingQuestions));
			setInitialQuestions(sortByDate(existingQuestions));
		} catch (error) {
			toast.error(
				"Something went wrong while getting the data from the server"
			);
		} finally {
			setIsLoading(false);
		}
	}, []);

	const onQuestionSave = async (question) => {
		try {
			setIsLoading(true);

			const savedQuestionR = await QuestionService.saveQuestion(question);
			const savedQuestion = savedQuestionR.data;

			const answerR = await AnswerService.getAnswersByQuestion(
				savedQuestion.id
			);

			savedQuestion.answerCount = answerR.data.length;

			setQuestions((prevQuestions) => {
				return sortByDate([...prevQuestions, savedQuestion]);
			});
		} catch (error) {
			toast.error("Something went wrong while adding the question");
		} finally {
			setIsLoading(false);
		}
	};

	return (
		<Grid>
			{isLoading && (
				<Dimmer active>
					<Loader />
				</Dimmer>
			)}
			<Header
				as="h1"
				style={{ margin: "0 auto", marginBottom: 20, marginTop: 20 }}
			>
				All Questions
			</Header>
			<Grid.Row centered>
				<QuestionFilter
					initialQuestions={initialQuestions}
					changeQuestions={setQuestions}
				/>
			</Grid.Row>
			<Grid.Row centered>
				<Button
					content="Ask a question"
					color="orange"
					onClick={() => setIsFormOpen(true)}
				/>
			</Grid.Row>
			<QuestionList questions={questions} />
			<QuestionFormPanel
				onClose={() => setIsFormOpen(false)}
				open={isFormOpen}
				onSave={onQuestionSave}
				userID={user ? user.id : 0}
				tags={tags}
			/>
			<ToastContainer />
		</Grid>
	);
};

export default QuestionsPage;
