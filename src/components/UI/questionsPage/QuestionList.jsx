import React from "react";
import { Label, Card, Header } from "semantic-ui-react";
import { Link } from "react-router-dom";

const QuestionList = (props) => {
	return (
		<Card.Group centered>
			{props.questions.length === 0 ? (
				<Header
					as="h2"
					style={{
						margin: "0",
						position: "absolute",
						top: "30%",
						left: "46%",
					}}
				>
					No results found
				</Header>
			) : (
				props.questions.map((question) => (
					<Card style={{ width: 1000 }} key={question.id}>
						<Card.Header>
							<Label color="orange">
								Votes:
								<Label.Detail>{question.upvotes}</Label.Detail>
							</Label>
							<Label color="black">
								Answers:
								<Label.Detail>
									{question.answerCount}
								</Label.Detail>
							</Label>
							<span
								style={{
									float: "right",
									marginRight: 20,
									fontSize: 13,
								}}
							>
								{question.date}
							</span>
						</Card.Header>
						<Card.Content textAlign="center">
							<Link
								to={{
									pathname: `/question/${question.id}`,
									state: { question: question },
								}}
								style={{fontSize: 20}}
							>
								{question.title}
							</Link>
							<Card.Meta textAlign="center">
								<span>Author: {question.author.username}</span>
							</Card.Meta>
							<Card.Description textAlign="left">
								{question.text}
							</Card.Description>
						</Card.Content>
						<Card.Content extra>
							{question.tagsList.map((tag) => (
								<Label as="a" key={tag.id}  tag>
									{tag.name}
								</Label>
							))}
						</Card.Content>
					</Card>
				))
			)}
		</Card.Group>
	);
};

export default QuestionList;
