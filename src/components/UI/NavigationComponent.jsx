import React, { useContext, useState } from "react";
import { useHistory } from "react-router";
import { Container, Menu, Segment } from "semantic-ui-react";
import { UserContext } from "../../resources/UserContext";

const NavigationComponent = () => {
	const [activeItem, setActiveItem] = useState("home");
	const { setUser } = useContext(UserContext);

	let history = useHistory();

	const handleItemClick = (e, { name }) => {
		setActiveItem(name);
		history.push(`/${name}`);
	};

	const handleLogout = (e, { name }) => {
		setUser(null);
		history.push("/login");
	};

	return (
		<Segment inverted>
			<Container>
				<Menu inverted pointing secondary>
					<Menu.Item
						name="questions"
						active={activeItem === "questions"}
						onClick={handleItemClick}
					/>
					<Menu.Item
						name="tags"
						active={activeItem === "tags"}
						onClick={handleItemClick}
					/>
					<Menu.Item
						name="logout"
						active={activeItem === "logout"}
						onClick={handleLogout}
						position="right"
					/>
				</Menu>
			</Container>
		</Segment>
	);
};

export default NavigationComponent;
