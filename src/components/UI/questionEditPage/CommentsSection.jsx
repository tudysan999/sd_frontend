import React, { useContext, useState } from "react";
import { toast } from "react-toastify";
import { Header, Comment, Form, Button, Icon, Label } from "semantic-ui-react";
import { UserContext } from "../../../resources/UserContext";
import UserService from "../../../services/UserService";
import Identicon from 'react-identicons';
import emailjs from "emailjs-com";

const CommentsSection = ({
	answers,
	onAnswerSave,
	onAnswerDelete,
	onAnswerUpdate,
	question,
}) => {
	const { user, setUser } = useContext(UserContext);
	const [answerText, setAnswerText] = useState("");
	const [editAnswer, setEditAnswer] = useState(null);

	const onAnswerSaveClicked = async () => {
		if (answerText.trim().length === 0) {
			return;
		}

		if (editAnswer) {
			onAnswerUpdate({ ...editAnswer, text: answerText });
			setAnswerText("");
			setEditAnswer(null);
			return;
		}
		if(!user){
			toast.warning("You must be logged in");

			return;
		}

		const currentUserR = await UserService.getUser(user.id);

		const newAnswer = {
			text: answerText,
			upvotes: 0,
			author: currentUserR.data,
			question: question,
		};

		onAnswerSave(newAnswer);
		setAnswerText("");
	};

	const onAnswerEditClicked = (answer) => {
		if (!user.admin && user.id !== answer.author.id) {
			toast.warning("You must be the owner of the answer in order to edit it.");
			return;
		}

		setAnswerText(answer.text);
		setEditAnswer(answer);
	};

	const onAnswerDeleteClicked = (answer) => {
		if (!user.admin && user.id !== answer.userID) {
			toast.warning(
				"You must be the owner of the answer in order to delete it."
			);
			return;
		}

		onAnswerDelete(answer.id);
	};

	const onVoteAnswer = async (answer, value) => {
		if (user.id === answer.author.id) {
			toast.warning("You can not vote your own answer");
			return;
		}
		
		if(value < 0) {
			const newUserR = await UserService.updateUser({...user, score: user.score - 1})
			setUser(newUserR.data)
		}

		onAnswerUpdate({ ...answer, upvotes: answer.upvotes + value });
	};

	const onBanUser = async (answer) => {
		if(answer.author.admin || answer.author.id === user.id){
			toast.warning("Admins cannot be banned");
			return;
		}

		await UserService.updateUser({...answer.author, isBanned: true});
		emailjs.send("service_fqfjxh8","template_ormfve7",{to_name: answer.author.username, to_email: answer.author.email},"YaMSBDV0KunH2g_WR")
		toast.success(`User ${answer.author.username} banned successfully`);
	}

	return (
		<Comment.Group 
			style={{ margin: "0 auto", paddingTop: "5%", maxWidth: "inherit" }}
			size="huge"
		>
			<Header as="h3" dividing>
				Answers
			</Header>

			{answers.map((answer) => (
				<Comment>
					<Button.Group floated="right" size="mini">
						<Button
							size="mini"
							onClick={() => onVoteAnswer(answer, -1)}
							color="black"
						>
							<Icon name="minus" />
						</Button>
						<Button.Or size="mini" text={answer.upvotes}/>
						<Button
							size="mini"
							onClick={() => onVoteAnswer(answer, 1)}
							color="orange"
						>
							<Icon name="plus" color="black" />
						</Button>
					</Button.Group>
					<Comment.Avatar src={<Identicon string={answer.author.id} size={50}/>} />
					<Comment.Content>
						<Comment.Author as="a">
							{answer.author.username}
							<Label circular color="orange">{answer.author.score}</Label>
						</Comment.Author>
						
						<Comment.Text>{answer.text}</Comment.Text>
						<Comment.Actions>
							<Comment.Action
								onClick={() => onAnswerEditClicked(answer)}
							>
								Edit
							</Comment.Action>
							<Comment.Action
								onClick={() => onAnswerDeleteClicked(answer)}
							>
								Delete
							</Comment.Action>
							{user.admin && <Comment.Action
								onClick={() => onBanUser(answer)}
							>
								Ban User
							</Comment.Action>}
						</Comment.Actions>
					</Comment.Content>
				</Comment>
			))}

			<Form >
				<Form.TextArea
					value={answerText}
					onChange={(event, data) => setAnswerText(data.value)}
				/>
				<Button
					style={{backgroundColor: "#f2711c", color: "black"}}
					content={
						editAnswer ? "Edit Your Answer" : "Post Your Answer"
					}
					labelPosition="left"
					icon="edit"
					primary
					onClick={onAnswerSaveClicked}
				/>
			</Form>
			
		</Comment.Group>
		
	);
};

export default CommentsSection;
